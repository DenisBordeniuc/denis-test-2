public class ex6 {
    public static void main(String[] args) {

        String arti = "Azi o dat zarplata ";

        int index = arti.indexOf('r');

        System.out.println("Cautam indexul literei r: " + index);

        int lastIndex = arti.lastIndexOf('r');

        System.out.println("Ultima aparitie a cuvintului r: " +  lastIndex);
    }

}
